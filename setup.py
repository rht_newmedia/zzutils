from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'DESCRIPTION.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='zzutils',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version='0.1.0',

    description='Common global utilities',
    long_description=long_description,

    # The project's main homepage.
    url='https://bitbucket.org/bowe_strickland/zzutil',

    # Author details
    author='Bowe Strickland',
    author_email='bowe@redhat.com',

    # Choose your license
    license='GPLv2',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Red Hat Internal',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
    ],
    keywords='',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=[
    ],
    extras_require={
        'dev': ['check-manifest'],
        'test': ['nose', 'coverage'],
    },

    package_data={
        #'sample': ['package_data.dat'],
    },
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    #data_files=[('my_data', ['data/data_file'])],

    entry_points={
    #    'console_scripts': [
    #        #'pezes=pezes.cli:app_cli',
    #    ],
    },
    test_suite = 'nose.collector',
)
