#!/usr/bin/python
#
#  Copyright (c) 2014 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os
from contextlib import contextmanager
from subprocess import Popen, PIPE, CalledProcessError

from .tests.base import BaseTest     # for export

try: 
    import yaml
    from .yaml_utils import *       # pragma: nocover
except ImportError:                 # pragma: nocover
    DEBUG("no yaml library discovered.")

def trim(text, width=60):
    width = max(3, width)
    return text if len(text) < width else text[:width-3] + "..."

def prefix_text_lines(line_buffer, prefix):
    ll = line_buffer.split(os.linesep)
    return os.linesep.join([ prefix + l for l in ll])

@contextmanager
def indir(dirname, create=True):

    # on no dirname, do nothing
    if (not dirname) or (dirname == "."):
        yield
        return

    if not os.path.exists(dirname) and create:
        INFO("creating %s" % dirname)
        os.makedirs(dirname)

    cwd = os.getcwd()

    try:
        INFO("chdiring to %s" % dirname)
        os.chdir(dirname)
        yield
    finally:
        INFO("returning to %s" % cwd)
        os.chdir(cwd)

def partition(xx, predicate):

    true_l,false_l = [], []

    for x in xx:
        if predicate(x):
            true_l.append(x)
        else:
            false_l.append(x)

    return true_l, false_l

def run(cmd, input="", **kw):

    INFO("running '%s' %s" % (cmd, kw))

    p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate(input)
    rv = p.wait()

    INFO("return value: %s" % rv)
    INFO("stdout: %s" % trim(stdout))
    INFO("stderr: %s" % trim(stderr))

    return rv, stdout, stderr

def run_check(cmd, input="", **kw):
    rv, stdout, stderr = run(cmd, input, **kw)
    if rv != 0:
        WARN("command failed: %s" % rv)
        WARN("stdout: %s" % stdout)
        WARN("stderr: %s" % stderr)
        raise CalledProcessError(rv, cmd, stdout)
    return stdout, stderr

