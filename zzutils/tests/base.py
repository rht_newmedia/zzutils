#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os
import unittest

class BaseTest(unittest.TestCase):

    def __init__(self, *args, **kw):
        super(BaseTest, self).__init__(*args, **kw)
        self._cleanups = []

    def tearDown(self):

        for cb, args, kwargs in self._cleanups:
            cb(*args, **kwargs)

        super(BaseTest, self).tearDown()

    def add_cleanup(self, cb, args=None, kwargs=None):
        
        args = args or []
        kwargs = kwargs or {}

        self._cleanups.append((cb, args, kwargs))

if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)
    import sys

# vi: ts=4 expandtab


