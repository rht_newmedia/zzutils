#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os
import unittest
from tempfile import NamedTemporaryFile, mkdtemp
from subprocess import CalledProcessError

from nose.tools import *
from .base import BaseTest
from .. import *

class TestUtils(BaseTest):

    def test_trim(self):

        text = "abcdefghijklmnopqrstuvwzyz"
        text_out = trim(text, 30)
        eq_(text, text_out)

    def test_trim_6(self):

        text = "abcdefghijklmnopqrstuvwzyz"
        text_out = trim(text, 6)
        eq_(text[:3]+"...", text_out)

    def test_prefix_text_lines(self):
        txt = "one\ntwo\nthree"
        txt = prefix_text_lines(txt, "foo ")
        eq_(txt, "foo one\nfoo two\nfoo three")

    def test_indir(self):

        dirname = mkdtemp()
        with indir(dirname):
            pass

        with indir("."):
            pass

        os.rmdir(dirname)

    def test_indir_mkdir(self):

        dirname = mkdtemp()
        with indir(dirname):
            with indir("foodir", create=True):
                pass
            os.rmdir("foodir")
        os.rmdir(dirname)


    def test_partition(self):

        lall = [ 1, 2, 3, 4, 5, 6 ]
        leven = [ 2, 4, 6 ]
        lodd = [ 1, 3, 5, ]

        lodd_out, leven_out = partition(lall, lambda x: x%2)
        eq_(leven_out, leven)
        eq_(lodd_out, lodd)

    def test_run_check(self):

        stdout, stderr = run_check("/usr/bin/echo hello".split())
        eq_(stdout.strip(), "hello")

    @raises(CalledProcessError)
    def test_run_check_bad(self):
        stdout, stderr = run_check("cat /rav/pmt/oof".split())



# vi: ts=4 expandtab


