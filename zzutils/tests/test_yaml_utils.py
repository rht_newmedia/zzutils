#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os
import unittest
from tempfile import NamedTemporaryFile, mkdtemp
from subprocess import CalledProcessError

from nose.tools import *
from .base import BaseTest
from .. import *

try:
    _HAS_YAML=True
    import yaml
except ImportError, e:      # pragma: nocover
    _HAS_YAML=False


# only define test class if PyYAML is available...
if _HAS_YAML:

    class TestYamlUtils(BaseTest):

        yaml_dict = """
            foo: bar
            biz: 
                bizfoo: baz
                bizbaz: 3
        """.strip()
        
        yaml_list = """
            - foo
            - bar
        """.strip()
        
        yaml_bad = """
            foo: bar:
        """.strip()
    
        def test_yaml(self):
    
            with NamedTemporaryFile() as f:
    
                data = { 'foo': "bar" }
                write_yaml_file(data, f.name)
                data_in = read_yaml_file(f.name)
                eq_(data, data_in)
    
        def test_yaml_stream(self):
    
            with NamedTemporaryFile() as f:
    
                data = { 'foo': "bar" }
                write_yaml_file(data, f)
                f.flush()
                f.seek(0)
                data_in = read_yaml_file(f)
                eq_(data, data_in)
    
        def test_as_yaml(self):
    
            obj = {
                'foo': u"bar",
            }
            txt = as_yaml(obj).strip()
            eq_(txt, "foo: 'bar'")
    
        def test_as_yaml_unicode(self):
    
            obj = {
                'foo': u"bar",
            }
            txt = as_yaml(obj, ignore_unicode_class=False).strip()
            eq_(txt, "foo: !!python/unicode 'bar'")
    
        def test_prefix_text_lines(self):
            txt = "one\ntwo\nthree"
            txt = prefix_text_lines(txt, "foo ")
            eq_(txt, "foo one\nfoo two\nfoo three")
    
        @raises(ValueError)
        def test_bad_yaml(self):
    
            with NamedTemporaryFile() as f:
    
                f.write(self.yaml_bad)
                f.flush()
                f.seek(0)
                data_in = read_yaml_file(f)
    
        @raises(ValueError)
        def test_yaml_not_dict(self):
    
            with NamedTemporaryFile() as f:
    
                write_yaml_file("one two".split(), f)
                f.flush()
                f.seek(0)
                data_in = read_yaml_file(f)
    



# vi: ts=4 expandtab


