#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
Utilities that depend on PyYAML library.
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import yaml

def as_yaml(obj, ignore_unicode_class=True):

    """ most often used to dump objects for human browsing, in which 
        case '!!python/unicode' string is annoying.

        If used for true storage with intent to reload, unicode 
        class defs should be preserved.
    """
    txt = yaml.dump(obj, default_flow_style=False)
    if ignore_unicode_class:
        txt = txt.replace("!!python/unicode ", "")
    return txt

def write_yaml_file(data, file):

    kw = { 'default_flow_style': False, 'encoding': "utf-8" }
    if isinstance(file, basestring):
        with open(file, "w") as f:
            f.write(yaml.dump(data, **kw))
    else:
        file.write(yaml.dump(data, **kw))
        
def read_yaml_file(filename_or_stream, force_dict=True):

    local_open = False
    if isinstance(filename_or_stream, basestring):
        f = open(filename_or_stream)
        local_open = True
    else:
        f = filename_or_stream
        
    try:
        obj = yaml.load(f)
    except yaml.YAMLError, e:
        msg = "could not parse YAML file %s: %s" % (filename_or_stream, e)
        raise ValueError(msg)

    if local_open:
        f.close()

    if force_dict and not isinstance(obj, dict):
        msg = "YAML file %s does not represent a dictionary"
        raise ValueError(msg % filename_or_stream)

    return obj


# vi: ts=4 expandtab


